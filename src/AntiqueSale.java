public class AntiqueSale {
    // Stage 1A - Instance Variables
    private String itemNumber;
    private String description;
    private boolean takingOffers;
    private int currentOffer;
    private int reservePrice;

    // Stage 1B - implement constructor for AntiqueSale class
    public AntiqueSale(String itemNumber, String description, int reservePrice) {
        this.itemNumber = itemNumber;
        this.description = description;
        this.takingOffers = true;
        this.currentOffer = 0;
        this.reservePrice = reservePrice;
    }

    // Stage 1C - implement accessors
    public String getItemNumber() {
        return itemNumber;
    }

    public String getDescription() {
        return description;
    }

    public boolean getTakingOffers() {
        return takingOffers;
    }

    // Stage 1D - calculate and return the brokerage fee
    public double calculateFee() {
        if (takingOffers) {
            return 0;
        }

        if (reservePrice > 0) {
            return (reservePrice * 0.05) + (currentOffer * 0.05);
        }

        return currentOffer * 0.10;
    }

    //Stage 1E - implement final offer for a 'no reserve' sale
    public boolean acceptOffer() {
        if (reservePrice > 0 || !takingOffers) {
            return false;
        }

        takingOffers = false;

        return true;
    }

    //Stage 1F - take offers for antique sale
    public boolean takeOffer(int offerPrice) {
        if (!takingOffers || offerPrice <= currentOffer) {
            return false;
        }

        currentOffer = offerPrice;

        if (reservePrice > 0 && currentOffer >= reservePrice) {
            takingOffers = false;
        }

        return true;

    }


    // Print sales report summary
    public void printDetails() {
        System.out.printf("Item Number: %s, Description: %s\n", itemNumber, description);
        System.out.printf("Taking Offers: %b, Current Offer: %d, Reserve: %d\n\n", takingOffers, currentOffer, reservePrice);

    }

}
   
