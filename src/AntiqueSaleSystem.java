import javax.xml.soap.SOAPPart;
import java.util.Scanner;

public class AntiqueSaleSystem {
    // create a Scanner
    private static Scanner input = new Scanner(System.in);

    // declare AntiqueSales array and object count
    private static AntiqueSale[] sales = new AntiqueSale[8];

    public static void main(String[] args) {

        String selection;

        do {

            // display menu options
            System.out.println("   ***** Antique Sale System Menu *****");
            System.out.println("A. Create / Reset Antique Sales");
            System.out.println("B. Display All Antique Sales");
            System.out.println("C. Display Sales Report");
            System.out.println("D. Take Offer");
            System.out.println("E. Accept Offer");
            System.out.println("X. Exit the program");
            System.out.println();

            // prompt user to enter selection
            System.out.print("Enter selection: ");
            selection = input.nextLine();

            System.out.println();

            // validate selection input length
            if (selection.length() != 1) {
                System.out.println("Error - invalid selection!");
            } else {

                // process user's selection
                switch (selection.toUpperCase()) {
                    case "A":
                        createAntiqueSales();
                        break;

                    case "B":
                        displayAllSales();
                        break;

                    case "C":
                        displaySalesReport();
                        break;

                    case "D":
                        takeOffer();
                        break;

                    case "E":
                        acceptOffer();
                        break;

                    case "X":
                        System.out.println("Exiting the program...");
                        break;

                    default:
                        System.out.println("Error - invalid selection!");
                }
            }
            System.out.println();

        } while (!selection.equalsIgnoreCase("X"));
    }

    public static void createAntiqueSales() {

        System.out.println("Creating / Resetting Antique Sales Objects");
        System.out.println();

        // code for Stage 2A should go in here
        sales[0] = new AntiqueSale("VSB01", "Victorian Mahogany Sideboard", 4000);
        sales[1] = new AntiqueSale("FMS01", "Federation Meat Safe", 0);
        sales[2] = new AntiqueSale("EBD01", "Edwardian White + Brass Double Bed", 0);
        sales[3] = new AntiqueSale("GFC01", "1830's Mitchell Grandfather Clock", 3000);
        sales[4] = new AntiqueSale("VBD01", "French Mahogany + Brass Double Bed", 0);
        sales[5] = new AntiqueSale("VSB02", "Victorian Era Walnut Sideboard", 6000);
        sales[6] = new AntiqueSale("VCR01", "1920 Model T Ford", 0);
        sales[7] = new AntiqueSale("EDR01", "18th Century Welsh Elm Dresser", 25000);

    }

    public static void displayAllSales() {
        System.out.println("Full Antique Sales List:");
        System.out.println();

        // code for Stage 2B should go in here
        for (AntiqueSale sale : sales) {
            sale.printDetails();
        }
    }

    private static void displaySalesReport() {

        System.out.println("Current Antique Sales Report:");
        System.out.println();

        // code for Stage 2C should go in here

        double totalBrokerageFees = 0.0;

        for (AntiqueSale sale : sales) {
            System.out.printf("Item Number: %s\n", sale.getItemNumber());
            System.out.printf("Description: %s\n", sale.getDescription());
            System.out.printf("Brokerage Fee: %f\n\n\n", sale.calculateFee());
            totalBrokerageFees = totalBrokerageFees + sale.calculateFee();
        }

        System.out.printf("\n\nTotal Brokerage Fees: %f\n", totalBrokerageFees);
    }

    private static void takeOffer() {
        System.out.println("Take Offer Feature:");
        System.out.println();

        // code for Stage 2D should go in here

        // search item number
        String searchItemNumber;
        int offerPrice;
        AntiqueSale antique = null;
        boolean offerOutcome;

        System.out.print("Enter Item Number: ");
        searchItemNumber = input.nextLine();

        for (AntiqueSale sale : sales) {
            if (sale.getItemNumber().equalsIgnoreCase(searchItemNumber)) {
                if (sale.getTakingOffers()) {
                    antique = sale;
                    break;
                }

                System.out.println("Antique is not taking offers");
                return;
            }
        }

        if (antique == null) {
            System.out.println("Antique not found");
            return;
        }

        System.out.println("Enter offer: ");
        offerPrice = input.nextInt();
        input.nextLine();

        offerOutcome = antique.takeOffer(offerPrice);

        if (!offerOutcome) {
            System.out.println("Offer was rejected");
            return;
        }

        System.out.printf("New highest offer is successful @ %d\n", offerPrice);

        if (!antique.getTakingOffers()) {
            System.out.println("Auction is now closed");
        }

    }

    private static void acceptOffer() {
        System.out.println("Accept Offer Feature:");
        System.out.println();

        // code for Stage 2E should go in here
        String searchItemNumber;
        AntiqueSale antique = null;
        boolean offerOutcome;

        System.out.print("Enter Item Number: ");
        searchItemNumber = input.nextLine();

        for (AntiqueSale sale : sales) {
            if (sale.getItemNumber().equalsIgnoreCase(searchItemNumber)) {
                antique = sale;
            }
        }

        if (antique == null) {
            System.out.println("Antique not found");
            return;
        }

        offerOutcome = antique.acceptOffer();

        if (!offerOutcome) {
            System.out.println("Reserve price not met for " + antique.getItemNumber());
            return;
        }

        System.out.println("Offer accepted for " + antique.getItemNumber());

    }

}
